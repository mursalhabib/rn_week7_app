import React, {Component} from 'react';
import {Text, View, Dimensions} from 'react-native';
import axios from 'axios';

export class Genres extends Component {
  constructor(props) {
    super(props);
    this.state = {
      genres: [],
    };
  }

  componentDidMount = async () => {
    const getMovies = await axios.get(
      'http://code.aldipee.com/api/v1/movies/' + this.props.id,
    );
    this.setState({genres: getMovies.data.genres});
    // console.log(this.state.genres);
  };

  render() {
    const winWidth = Dimensions.get('window').width;
    return (
      <View
        style={{
          flex: 1,
          flexWrap: 'wrap',
          alignItems: 'flex-start',
          width: winWidth * 0.6,
          alignItems: 'center',
        }}>
        <Text>
          {this.state.genres.map(genre => (
            <Text
              key={genre.id}
              style={{
                color: '#fff',
                fontFamily: 'Nunito-Light',
              }}>
              {genre.name} {'  '}
            </Text>
          ))}
        </Text>
      </View>
    );
  }
}

export default Genres;
