import React, {Component} from 'react';
import {Text, View} from 'react-native';

export class Splash extends Component {
  componentDidMount() {
    setTimeout(() => {
      this.props.navigation.replace('List');
    }, 2500);
  }
  render() {
    return (
      <View
        style={{
          backgroundColor: '#22252a',
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontFamily: 'Pacifico-Regular',
              fontSize: 100,
              color: '#f24336',
            }}>
            {' '}
            M.{' '}
          </Text>
        </View>
        <Text
          style={{
            fontFamily: 'OpenSans-Black',
            fontSize: 15,
            color: '#6f7781',
          }}>
          {' '}
          FROM{' '}
        </Text>
        <Text
          style={{
            fontFamily: 'OpenSans-Black',
            fontSize: 15,
            color: '#4d5259',
            marginBottom: 10,
          }}>
          {' '}
          MURSAL HABIB{' '}
        </Text>
      </View>
    );
  }
}

export default Splash;
