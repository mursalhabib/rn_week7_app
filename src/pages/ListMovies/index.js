import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  Dimensions,
  RefreshControl,
  ScrollView,
  Alert,
} from 'react-native';
import axios from 'axios';
import Icon from 'react-native-vector-icons/AntDesign';
import moment from 'moment';
import 'moment/locale/id';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import NetInfo from '@react-native-community/netinfo';
import Genres from '../Genres';
import {Image} from 'react-native-elements';

export class ListMovies extends Component {
  NetInfoSubscription = null;

  constructor(props) {
    super(props);
    this.state = {
      dataMovies: [],
      isLoading: false,
      connectionStatus: false,
    };
  }

  componentDidMount = () => {
    this.NetInfoSubscription = NetInfo.addEventListener(
      this._handleConnectivityChange,
    );
    this.getDataMovies();
  };

  componentWillUnmount() {
    this.NetInfoSubscription && this.NetInfoSubscription();
  }

  _handleConnectivityChange = state => {
    this.setState({connectionStatus: state.isConnected});
  };

  getDataMovies = async () => {
    this.setState({isLoading: true});
    try {
      const getMovies = await axios.get(
        'http://code.aldipee.com/api/v1/movies/',
      );
      this.setState({dataMovies: getMovies.data.results});
      this.setState({isLoading: false});
    } catch (error) {
      Alert.alert(error);
    }
  };

  rekomendasiMovie = ({item}) => {
    return (
      <TouchableOpacity
        activeOpacity={0.6}
        onPress={() => this.props.navigation.navigate('Detail', item.id)}
        style={{
          padding: 10,
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <Image
          source={{uri: item.poster_path}}
          style={{height: 192, width: 108, borderRadius: 20}}
        />
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <>
        {this.state.isLoading == true ? (
          <View style={{flex: 1, zIndex: 100, backgroundColor: '#22252a'}}>
            <SkeletonPlaceholder
              backgroundColor={'#2d3035'}
              highlightColor={'#4d5259'}>
              <View style={Styles.logo} />
              <View style={Styles.recommend} />
              <View style={{flexDirection: 'row'}}>
                <View style={Styles.poster} />
                <View style={Styles.poster} />
                <View style={Styles.poster} />
                <View style={Styles.poster} />
              </View>
              <View style={Styles.latest} />
              <View style={{flexDirection: 'row'}}>
                <View style={Styles.poster} />
                <View style={{flexDirection: 'column'}}>
                  <View style={Styles.title} />
                  <View style={Styles.info} />
                  <View style={Styles.info} />
                  <View style={Styles.info} />
                  <View style={Styles.info} />
                </View>
              </View>
            </SkeletonPlaceholder>
          </View>
        ) : (
          <View style={Styles.container}>
            <ScrollView
              refreshControl={
                <RefreshControl
                  refreshing={this.state.isLoading}
                  onRefresh={this.getDataMovies}
                />
              }>
              {this.state.connectionStatus ? (
                <></>
              ) : (
                <View style={Styles.conn}>
                  <Icon name="warning" size={16} color="#fff" />
                  <Text style={Styles.offline}>you are offline</Text>
                </View>
              )}

              <Text style={Styles.mooovie}>
                M<Text style={{color: '#fff'}}>ovieeez</Text>
              </Text>

              <Text style={Styles.headerText}>Recommended</Text>
              <View>
                <FlatList
                  horizontal={true}
                  showsHorizontalScrollIndicator={false}
                  data={this.state.dataMovies
                    .sort((a, b) => {
                      return b.vote_average - a.vote_average;
                    })
                    .slice(0, 10)}
                  renderItem={this.rekomendasiMovie}
                  keyExtractor={(i, k) => k.toString()}
                />
              </View>
              <Text style={Styles.headerText}>Latest Upload</Text>
              <View style={{flex: 1}}>
                {this.state.dataMovies
                  .sort((a, b) => {
                    return new Date(b.release_date) - new Date(a.release_date);
                  })
                  .map(item => (
                    <View key={item.id} style={Styles.movieContainer}>
                      <Image
                        source={{uri: item.poster_path}}
                        style={{height: 192, width: 108, borderRadius: 20}}
                      />
                      <View style={Styles.infoContainer}>
                        <View
                          style={{
                            marginBottom: 5,
                          }}>
                          <Text
                            style={{
                              color: '#fff',
                              fontFamily: 'Nunito-Bold',
                              fontSize: 18,
                            }}>
                            {item.original_title}
                          </Text>
                        </View>
                        <Text
                          style={{
                            color: '#fff',
                            fontFamily: 'Nunito-Regular',
                            fontSize: 12,
                            marginBottom: 5,
                          }}>
                          {item.release_date == null
                            ? 'Coming Soon'
                            : moment(item.release_date).format('Do MMM YYYY')}
                        </Text>
                        <View
                          style={{
                            paddingVertical: 3,
                            paddingHorizontal: 5,
                            alignSelf: 'flex-start',
                            backgroundColor: '#3e4249',
                            borderRadius: 7,
                          }}>
                          {item.vote_average == 0 ? (
                            <Text
                              style={{
                                color: '#fff',
                                fontFamily: 'Nunito-Bold',
                              }}>
                              Not rated yet
                            </Text>
                          ) : (
                            <Text
                              style={{
                                color: '#fff',
                                fontFamily: 'Nunito-Bold',
                                fontSize: 12,
                              }}>
                              <Icon name="star" size={14} color="#FFDF00" />{' '}
                              {item.vote_average}
                            </Text>
                          )}
                        </View>
                        <Text
                          style={{
                            color: '#fff',
                            fontFamily: 'Nunito-Bold',
                            fontSize: 17,
                            marginBottom: 15,
                          }}>
                          <Genres id={item.id} />
                        </Text>
                        <TouchableOpacity
                          activeOpacity={0.6}
                          onPress={() =>
                            this.props.navigation.navigate('Detail', item.id)
                          }
                          style={Styles.showMore}>
                          <Text
                            style={{
                              color: '#fff',
                              fontFamily: 'Nunito-Bold',
                              fontSize: 14,
                            }}>
                            Show More
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  ))}
              </View>
            </ScrollView>
          </View>
        )}
      </>
    );
  }
}
export default ListMovies;

const winWidth = Dimensions.get('window').width;
const Styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: '#22252a'},
  conn: {
    marginTop: 10,
    borderRadius: 20,
    backgroundColor: '#b12318',
    width: '33%',
    alignSelf: 'center',
    padding: 3,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  offline: {
    textAlign: 'center',
    justifyContent: 'center',
    color: '#fff',
    marginLeft: 10,
  },
  mooovie: {
    fontSize: 30,
    color: '#f24336',
    marginLeft: 13,
    marginTop: 5,
    fontFamily: 'Pacifico-Regular',
  },
  headerText: {
    fontSize: 22,
    color: 'white',
    marginLeft: 13,
    marginTop: 15,
    fontFamily: 'Nunito-Bold',
  },
  logo: {
    marginTop: 20,
    marginLeft: 15,
    height: 40,
    width: 108,
    borderRadius: 7,
  },
  recommend: {
    marginTop: 20,
    marginLeft: 15,
    height: 25,
    width: 158,
    marginBottom: 15,
    borderRadius: 7,
  },
  poster: {
    height: 192,
    width: 108,
    marginHorizontal: 10,
    borderRadius: 20,
  },
  latest: {
    marginTop: 30,
    marginLeft: 15,
    height: 25,
    width: 143,
    marginBottom: 15,
    borderRadius: 7,
  },
  movieContainer: {
    flexDirection: 'row',
    padding: 10,
    alignItems: 'center',
  },
  infoContainer: {
    marginLeft: 20,
    alignItems: 'flex-start',
    flexWrap: 'wrap',
    width: winWidth * 0.7,
  },
  title: {
    marginTop: 30,
    marginLeft: 15,
    height: 25,
    width: 143,
    marginBottom: 10,
    borderRadius: 5,
  },
  info: {
    marginLeft: 15,
    height: 20,
    width: 103,
    marginBottom: 7,
    borderRadius: 5,
  },
  showMore: {
    backgroundColor: '#3e4249',
    alignSelf: 'flex-start',
    paddingHorizontal: 5,
    paddingVertical: 3,
    borderRadius: 10,
  },
});
