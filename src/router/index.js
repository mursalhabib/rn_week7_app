import React, {Component} from 'react';
import {Text, View, TouchableOpacity, Button} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import ListMovies from '../pages/ListMovies';
import DetailMovie from '../pages/DetailMovie';
import Splash from '../pages/Splash';
import Icon from 'react-native-vector-icons/Entypo';
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import Share from 'react-native-share';

export class Router extends Component {
  constructor() {
    super();
    this.state = {
      isLoved: false,
    };
  }

  handleLove = () => {
    this.setState({isLoved: !this.state.isLoved});
  };

  handleShare = async () => {
    const shareOptions = {
      message: `Hey, check out this movie!`,
    };
    try {
      await Share.open(shareOptions);
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    const Stack = createStackNavigator();
    return (
      <Stack.Navigator initialRouteName="Splash">
        <Stack.Screen
          name="List"
          component={ListMovies}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Detail"
          component={DetailMovie}
          options={{
            title: '',
            headerTransparent: true,
            headerTintColor: '#fff',
            headerLeftContainerStyle: {
              borderRadius: 10,
              left: 10,
              marginVertical: 10,
              backgroundColor: '#22252a',
            },
            headerRight: () => (
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <TouchableOpacity activeOpacity={0.8} onPress={this.handleLove}>
                  {this.state.isLoved == true ? (
                    <MaterialIcon
                      color="red"
                      size={23}
                      name="heart"
                      style={{
                        marginRight: 20,
                        backgroundColor: '#22252a',
                        borderRadius: 10,
                        padding: 5,
                      }}
                    />
                  ) : (
                    <MaterialIcon
                      color="#fff"
                      size={23}
                      name="heart"
                      style={{
                        marginRight: 20,
                        backgroundColor: '#22252a',
                        borderRadius: 10,
                        padding: 5,
                      }}
                    />
                  )}
                </TouchableOpacity>
                <TouchableOpacity
                  activeOpacity={0.8}
                  onPress={this.handleShare}>
                  <MaterialIcon
                    color="#fff"
                    size={25}
                    name="share"
                    style={{
                      marginRight: 20,
                      backgroundColor: '#22252a',
                      borderRadius: 10,
                      padding: 4,
                    }}
                  />
                </TouchableOpacity>
              </View>
            ),
          }}
        />
        <Stack.Screen
          name="Splash"
          component={Splash}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    );
  }
}

export default Router;
